Starting in 2015 with the these maintenance as craft, a critical approach of the notion of work to question our relationship with the environment, we believe that maintenance is architecture : an integral part of the designed and built environment that transforms it radically. To act accordingly, we ask ourselves what is maintained when we maintain and practice maintenance as a research method. We develop through storytelling, including research, drawings, writings, workshops, conferences, exhibitions and direct action. We do data analysis and image representation. From the maintenance industry to daily households, all over the wide spectrum of design, architecture and urbanism, MAMA calls for dinner.

More info on <http://mama.brussels/>

## Install
- clone the repository
- install dependencies: pelican & markdown

## Running

```
. path/to/venv/bin/activate
cd site
make devserver
```

## Small Markdown guide
##### Inserting an image
`![](images/filename.jpg)`

##### Inserting a logo
`![](images/filename.jpg){: .logo }`