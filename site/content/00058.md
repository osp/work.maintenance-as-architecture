Title: Akragas Temple 
Date: 31-05-2019
Tags: Gary please, temple, scaffolding
Location:BOSCH
Mood: day
Image: 00058 7 Akragas Temple scaffolding.pdf .svg

00058

Printed on a fabric covering the Concord temple in Sicily a representation of how the building could have looked like originally. Printing historic buildings on fabrics that cover construction works is a common practice. From the eye of a spectator, scaffolding systems and workers seem to disturb the volume, the beauty, the essence of the building.

Ironically, these works can greatly influence the longevity of the building. From our perspective, to hide these conservative processes is a counterproductive measure. It does not lead to a better understanding of the life cycle of the building neither the different practices that maintain it.





#  


