Title: Flowers out of cracks
Date: 3-12-2019
Tags: Gary Please, police, quote, article, book, theory, window, question
Location:Brussels
Mood: night
Image: 00123.svg

00123

**"A piece of property is abandoned, weeds grow up, a window is smashed. Adults stop scolding rowdy children; the children, emboldened, become more rowdy. Families move out, unattached adults move in. Teenagers gather in front of the corner store. The merchant asks them to move; they refuse. Fights occur. Litter accumulates. People start drinking in front of the grocery; in time, an inebriate slumps to the sidewalk and is allowed to sleep it off. Pedestrians are approached by panhandlers."**

*Extract coming from the original article written in 1982 by social scientists James Q. Wilson and George L. Kelling.* 

The "Broken Window Theory" has been mostly interpreted to reduce crime through police presence focusing on small crimes and has been contested for targeting low income communities therefore.

But based on the book *Palaces for the People: How Social Infrastructure Can Help Fight Inequality, Polarization, and the Decline of Civic Life*, Eric Kbinenberg points in The New Yorker with *The Other Side of* *Broken Windows* (August 23, 2018) what would happen if vacant property received the attention that, for decades, has been given to fight petty crime? 

Police strategies based on the "Broken Window Theory" seem to have been successful but leave many questions open on how the society does or can develop around these authority representatives. 

What if this theory is not used as a police strategy but rather as a city vacancy policy?  What if we support low incomes to care about their housing through financial and educational support, or by increasing the number of maintainers (gardeners, plumbers, painters, etc.) Who support these communities rather than police officers?

The drawing of this entry represents a broken window in Brussels. The cracks have been hidden by painting branches, leaves and flowers of diferent kinds.










