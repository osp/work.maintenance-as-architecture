Title: Office 

MAMA is a developing spatial practice that takes a critical look at our everyday relationship with the built environment. By exploring maintenance as craft, MAMA defends the notion of maintenance as a legitimate subject within architecture itself (a part of the designed and built environment that transforms it radically) and not merely a by-product or an inevitable consequence. MAMA believes it is fundamental to ask the question: What do we maintain when we practice maintenance? MAMA practices daily maintenance in different domains and claims it as a research method. 

MAMA has been engaged in promoting and coordinating appropriation of vacant buildings and transformation of public spaces with and through maintenance practices. 

MAMA has been taking part in several self-managed co-housing projects, often eclectic environments that challenge the dwelling norm, promoting alternative ways of consumption and proposing domestic spaces for common and public use. Thanks to these experiences, we developed an architectural practice that redefines the time schedule and process of architectural production and aims at establishing a profounder relationship between dwellers and their inhabited space though maintenance knowledge.

Contact : <info@mama.brussels> 

### Projects / Conferences / Publications 


<nav markdown="true" class="project-navigation">

* <a href="#globearoma"><font size="5">GlobeAroma</font></a> 
* <a href="#contrat"><font size="5">Contrat École</font></a> 
* <a href="#fms"><font size="5">FixMyStreet, The maintenance process as a design tool for public space</font></a> 
* <a href="#labor"> <font size="5">The Maintainers II & III</font></a>
* <a href="#permanent"><font size="5">Permanent</font></a>
*  <a href="#maintenance-as-research"><font size="5">Maintenance as research</font></a>
* <a href="#you-are-here"><font size="5">You Are Here - The Future is a Practice</font></a>
* <a href="#pleonasme"><font size="5">fig. n°4 - Pléonasme</font></a>
* <a href="#construire"><font size="5">Construire le monde</font></a>
* <a href="#outsiders"><font size="5">Outsiders, Occuper autrement le terrain de la recherche en design</font></a>
* <a href="#vision-futur"><font size="5">Visions du futur</font></a>
* <a href="#entretien"><font size="5">L'entretien e(s)t l'architecture / Maintenance as architecture</font></a>
* <a href="#onderhoud"><font size="5">Onderhoud als vak / Maintenance as craft</font></a>

</nav>

<aside markdown="true" id="office-bio">

<div class="portraits" markdown="true">

![](/images/portrait-koen.svg)
Koen Berghmans (1983) studied architecture at Sint-Lucas, Brussels. He has been assisting performing artists and is engaging in different causes, among which Potential Office Project (POP), that questions collaboration beyond the limits of disciplines, social occupations and precarity. In 2013 he held a retrospective exhibition. He met Bernardo within the collective of Rotor and they lived together within the housing community of Woningen 123 Logements.

![](/images/portrait-marianita.svg)
Maria Anita Palumbo (1981) PhD in social anthropology and ethnology at EHESS, She is an Associate Professor (Tenure) in Human and Social Sciences at the Saint-Etienne School of Architecture. She specializes in the relationship between city and alterity. Her field of inquiry are urban situations at the margins. Her research also questions the processes of urban transformation observed through ethnographic work. Between 2012 and 2018, she conducted research with the architect Olivier Boucheron titled “In-between slabs : the “infraordinary” of modernity” a global ethnography of the transformation of large collective housing projects by their dwellers. 

![](/images/portrait-bernardoo.svg)
Bernardo Robles Hidalgo (1984) studied architecture (ENSAPB) and project management for construction ( Brighton University). He has collaborated with the organisation Toestand as a coordinator, project assistant and CJO (chief janitor officer ), analysing the uses and redevelopment of a public space in Brussels during its temporary occupation in anticipation of the future development. He is a painter and illustrator; his work has been exhibited at the RAA (Royal Academy of Arts, London), WSC (Watercolor Society Competition, London) and Het Nieuwe Instituut (Rotterdam) amongst others. He worked as an illustrator and dismantler for Rotor. Presently developing a parental project with Marianita Palumbo at BOSCH (co-housing and shared domestic/community space in Brussels).

</div>

</aside>

### GlobeAroma {#globearoma}

![](images/globe.png)

Globe Aroma is an open arts house, offering time, creative support and a network for artists, creatives and culture lovers. It has many uses, among them: exhibition space, material storages, art storage, art studios. These uses are leaving traces that in occasions can conflict with the production and exhibition of new artistic works. 

During 2020 MAMA will be collaborating with Globe Aroma developing and implementing methodologies aiming to have an impact in the spatial configuration of the space, the valorisation of the artistic production and the behaviour of users.

→ [GlobeAroma](http://www.globearoma.be/)  


### Contrat École {#contrat}

![](images/ecolee.png)

For the first “Contrat École” promoted by Perspective.Brusselles, MAMA has joined CityTools, the architectural practices of a polyvalent team leading the project for the regional urban renewal program. This project aims at improving the school environment in Brussels and to strengthening the relations between schools and their neighborhood.

The “Contrat École” pursues a triple objective: 1) improve the urban integration of schools; 2) increase the supply of community facilities to neighborhood residents by opening schools outside of school hours 3) promote the opening of the school to the district through socio-economic actions and operations to reclassify public space.

WIthin this frame, the mission of MAMA is to analyze the state of the space from the point of view of wear, user habits, practices and professional figures exercising maintenance. This diagnostic work on the relationship between uses and the built space will contribute to design a new school-neighborhood interpenetration.

### FixMyStreet, The maintenance process as a design tool for public space {#fms}

![](images/fixmystreet.png)

In this research, we are interested in FixMyStreet.Brussels, website and mobile application developed by the CIRB (Center d'Informatique pour la Région Bruxelloise), as a tool for maintaining public space.

FixMyStreet is today a portal which is used to report incidents in Brussels public space and their resolution. In this research we analyze the data collected by this portal and their path towards the resolution of the reported problem in order to assess the data exploitation potential generated by Fix My Street for the improvement of the design, implementation and maintenance of public space.

The project aims to participate in the design and creation of qualitative and sustainable public spaces by understanding the life cycle of the elements and materials that make up Brussels public spaces (benches, lawns, lanterns, fences, street markings, pavements floors, poles ...) as well as by studying different services, practices and maintenance habits.

This clearly aims to improve the quality of life of residents and users by improving the framework and life cycle of public spaces in the Brussels-Capital Region

→ [fixmystreet.brussels](https://fixmystreet.brussels)  
→ [Brussels Mobility](https://mobilite-mobiliteit.brussels/en)

### Maintainers II ( Labor, Technology and Social Orders) New Jersey & Maintainers III ( Practice, Policy and Care ) Washington DC {#labor}

![](images/bible.jpg)
Museum of the Bible 400 4th St SW, Washington, DC 20024, United States

To get engaged with other maintenance communities around the world MAMA presented their work at MII ( Labor, Technology, and Social Orders ) 2017 and started a collaboration with The Maintainers for the construction of MIII ( Practice, Policy and Care ) 2019. The Maintainers is a global research network aiming at “maintaining self and society through reflection, research, and advocacy in the hopes of achieving a more caring and well-maintained world“. Some of the individuals working on the project are: Jessica Meyerson, Andrew Russell, Shannon Mattern, Lee Vinsel, Raquel Velho among others.

→ [Maintainers II](http://themaintainers.org/maintainers-ii-program/)  
→ [Maintainers III](<http://themaintainers.org/miii>)

### Permanent {#permanent}

Permanent is a practice-based research with the aim to develop an infrastructure for permanently affordable workspaces for artists and artworkers in Brussels. By investigating alternative understandings of ownership and by looking into new legal and financial models proposed by cooperatives and other “commoning” initiatives, Permanent wants to develop this much-needed infrastructure for artistic creation in Brussels without contributing to a process of gentrification and profit-driven urban development. The research puts forward the Community Land Trust-model: a social real estate development model in which the land remains collective property of the community and only the actual buildings can be acquired, at a lower price. These buildings can only be resold with limited profit which keeps them affordable in the long run and effectively cancels out financial speculation. The model has already demonstrated its capacity in many different places to include a larger and more diverse group of people in the development of an urban living environment beneficial for all. Permanent therefore also strives for radical solidarity with other groups excluded by speculative urban development schemes. It proposes a building programme that complements artistic workspaces with public neighbourhood infrastructure and affordable homes for people with limited means, installing new regimes of solidarity outside of the nuclear family. This way, Permanent aims to contribute in a pro-active and sustainable way to the urban city life in Brussels.

Permanent is a collaboration between Level Five, 431, MAMA, Arp:, Overtoon, Jubilee, That Might Be Right, Louiza and individual artists and artworkers temporarily based in the former Actiris-building at the Place de la Bourse in Brussels, with the support of Community Land Trust Brussels. The core team meets every first Monday of the month at Level Five and is open for everyone who wants to contribute and collaborate.

→ [Permanent](https://www.permanentbrussels.org/)
### Maintenance as research {#maintenance-as-research}

Development of a third step in a row of statements, the idea to practice
maintenance as a research method. With the support of the Flemish Government
for 2018.

### You Are Here - The Future is a Practice {#you-are-here}

![](images/youarehere.png)

Group exhibition in the temporarily occupied World Trade Center in Brussels, a
satellite of the International Architecture Biennial of Rotterdam,  IABR 2018+2020-The Missing Link. According to the curator
Architecture Workroom Brussels (AWB) MAMA and 20 other participants represent
“the missing link” for a common practice towards a more sustainable world.
From the 2<sup>nd</sup> of June until the 11<sup>th</sup> of November 2018.

→ [You Are Here - The Future is a Practice](http://www.youarehere.brussels)  
→ [IABR 2018+2020 - The Missing Link](https://iabr.nl/nl/editie/yah2018)

### fig. n°4 - Pléonasme {#pleonasme}
![](images/fig.png)
Publication of a selection from our exhibition’s catalogue L’entretien e(s)t
l’architecture / Maintenance as architecture by the redaction of the French
architecture magazine fig. in their 4th edition, on the theme of pleonasm.
Spring 2018.

→ [Revue-fig](http://www.revue-fig.com/collection)  




### Construire le monde {#construire}

Presentation, together with anthropologist Marianita Palumbo, assistant
professor at the Ecole Nationale Supérieure d’Architecture Saint-Etienne
(ENSASE), about the mix of pedagogical action, fieldwork and exhibition in our
practice at a seminary on the trans-disciplinary tools of scientific research,
more precisely in anthropology and architecture. Invited by the Centre d’Etudes
Sociologiques (CES) of the Université Saint-Louis Bruxelles (USL-B) and the CES
Sasha of the Faculté d’Architecture La Cambre-Horta of the Université Libre de
Bruxelles (ULB). On the 5th of December 2017.

→ [Université Saint-Louis Bruxelles (USL-B)](http://www.ces.usaintlouis.be/event/seminaire-du-ces-construire-le-monde)  
→ [Faculté d'Architecture La Cambre-Horta of the Université Libre de Bruxelles](http://archi.ulb.ac.be/actualites/2017-12-05/construire-le-monde)

### Outsiders, Occuper autrement le terrain de la recherche en design {#outsiders}

Presentation at a study day on alternative ways of doing research by design,
in preparation of the international colloquium La recherche en design : occuper
les marges in spring 2018. Invited by the Unité de Recherche design et création
of the Ecole Supérieure d’Art et Design Saint-Etienne (ESADSE) and the Pôle
recherche of the Cité du design. On the 30th of November 2017.

→ [Outsiders](http://esadse.fr/fr/outsiders/091117-programme?na#pitchcamp)  
→ [Facebook link](https://www.facebook.com/events/1936093709752899/)

### Visions du futur {#vision-futur}

Presentation at a cycle of conferences curated by the French architecture group
Zerm for the Ecole Nationale Supérieure d’Architecture et de Paysage de Lille
(ENSAPL). On the 10th of October 2017.

→ [Visions du futur](http://www.lille.archi.fr/index.php?ID=2062961)  
→ [Facebook link](https://www.facebook.com/events/138230610242806/)

### L'entretien e(s)t l'architecture / Maintenance as architecture {#entretien}

![](images/mama-maxime-disy.jpg)
Maxime Disy (c)

Exhibition on the evolving interaction between maintenance and the design of
public space at the 10th Biennale Internationale Design Saint-Etienne 2017 -
Working Promesse, shifting work paradigms.

Saint-Etienne, nicknamed the «black city» at the height of its industrial era for its predominantly polluted atmosphere, has become known since 2010 as a City of Design and a member of the UNESCO Creative Cities Network. Today, in stark contrast to its former reputation of a city in decline, it exudes the image of a city where «cleanliness and maintenance of the public space are a priority» as explains the city mayor in the Plan de Mandat 2014 - 2020. 

Our exhibition,  as a result of our *in situ* research,  illustrates various maintenance tasks carried out in the town of Saint-Etienne. As a catalog of the continuous performance of the maintenance of public space, the work invites the public to consider the permanent exhibition that happens outside, every day, in the city, and to consider each “maintainer” a "space-maker” and every inhabitant a “city-keeper.”  It aims at questioning any acts of government as an act of maintenance and the organization of the maintenance tasks as an act of city governance. 

Moreover, the question of maintenance, with its branched organization, highlights the changes in our structures of employment, including the subcontracting of public space management, the evolution of tools, our definition of what constitutes «good practice», the effects of standardization and, more generally, the transformation of careers through time, in Saint-Etienne and elsewhere. 

Fruit of the research on the maintenance of the city of Saint-Etienne’s public
space, in collaboration with anthropologist Marianita Palumbo, professor at the
Ecole Nationale Supérieure d’Architecture Saint-Etienne (ENSASE), and including a workshop and presentation for students of architecture. With the support of the Flemish Government, the Ecole Nationale Supérieure d’Architecture de Saint-Etienne (ENSASE) and the Biennale Internationale Design Saint-Etienne. Winter 2016-2017 (research) and from the 9th of March until the 9th of April 2017 (exhibition).

→ [L'entretien e(s)t l'architecture / Maintenance as architecture](https://www.biennale-design.com/saint-etienne/2017/fr/programme/?ev=architecture-du-travail-12)  
→ [Link to ENSASE's mediatheque](https://media.st-etienne.archi.fr/)

### Onderhoud als vak / Maintenance as craft {#onderhoud}

![](/images/maintenenace-as-craft-mama.jpg)
Finalist proposal of the open call by the Vlaams Architectuurinstituut (VAi) for
the Belgian pavilion at the 15th International Venice Architecture Biennale 2016-
Reporting from the front, on the theme of “crafts(wo)manschip” and the spaces
of the city, linking labour critic with architecture. Together with
Vincent P. Alexis, Livia Cahn, Edoardo Cimadori, Ronan Dériez, Elke Gutiérrez,
Kobe Matthys, Anna Rispoli, Tim Rottiers, Giulia Caterina Verga and
Potential Office Project (POP). Summer 2015.

ref: 00135

Text: Koen Berghmans

Translation from Dutch to English: Livia Cahn

"*I don't get why people don't like to clean. Is there any better way to get to know a house?*"

*(M.T., architect and housewife.)*

Maintenance can teach us architects a lot about matter, time and space.

The direct relationship we uphold with material through maintenance sustains us and is all together different than a functional relationship to materials. It’s more intuitive. In part also because of the object itself. On every level, frequent maintenance is necessary, from the dusty mantelpiece, to a roof tile damaged by wind and rain. *(In the original Dutch proposal the comparison was made between a cupboard and a complete building, referring to the difference in scale.)* Maintenance ensures that we feel good in a space, with the objects we use.

Maintenance is ephemeral. It's done, and needs to happen again and again, requiring time and attention. We learn it as we go along. It’s therefore seemingly antagonistic to durability, but this couldn’t be further from the truth. Although it is invisible to an outsider, maintenance nevertheless contributes to their experience. It's sensorial. It evaporates, like the smell of bleach after mopping. Or it is slippery like a brown soaped floor. Maintenance says something about the way we use available resources.

A particular form of maintenance is housekeeping. In Dutch "Huishouden" *("Huishouden" is best translated as housekeeping)* means as much as management or government, but applied to the use of space we could understand it as the maintenance of and inside built space. Popular wisdom tells us that household chores take place in the intimate environment of one's own home. "Huishouden" *(in this sentence "huishouden" could be translated as household)* also stands for domicile, but it's meaning can also reach further, consider water management for example. Lastly, "huishouden" equally means wreaking havoc, like a destructive hurricane approaching a mass of land. This word merges people and a very particular, yet momentous experience of space.

Housekeeping has its own logic. The one of the *experience expert* that is. We see chaos in other’s order and just like zero management in a nature reserve, not maintaining and simple use is also a form of housekeeping. Nothing is as personal as housekeeping. And nothing is as intimate. Housekeeping is what we rarely grasp consciously, though certainly unconsciously. It is nestled in your very being, by imitation, repetition and habituation.

When we talk about housekeeping, everyone obviously wants to achieve good work. You, yourself are the objective standard for it, after all. We maintain the space as if it were our body and many take care of it with more love. Housekeeping is a craft or as Sennett says *(Richard Sennett is the author of the book The Craftsman, which inspired my definition of 'crafts(wo)manship')*, we do it better as a craft.

ex: "*"Craftsmanship" may suggest a way of life that waned with the advent of industrial society - but this is misleading. Craftsmanship names an enduring, basic human impulse, the desire to do a job well for its own sake. Craftsmanship cuts a far wider swath than skilled manual labor; it serves the computer programmer, the doctor and the artist; parenting improves when it is practiced as a skilled craft, as does citizenship. In all these domains craftsmanship focuses on objective standards, on the thing in itself. Social and economic conditions, however, often stands in the way of the craftsman's discipline and commitment ... The argument here is that motivation matters more than talent, and for a particular reason. ... We are more likely to fail as craftsmen, I argue, due to our inability to organize obsession than because of our lack of ability.*"

*(Prologue : Man as His Own Maker. In : SENNET, R. 2008. The Craftsman. Yale University Press, New Haven & London, 9-11)*

But there are threats posed to that possible craftsmanship.

A craft is not work or work is not the same as a craft. A professional service doesn't mean craftsmanship *per se*. Although a craftsman is often presented as the alternative for clumsy do-it-yourself repair, this is not valid for maintenance. We do things better ourselves when we don't just want to get rid of it. On the labour market a craftsman stands for competent. Competences are written in CV's, but for many jobs in the maintenance sector, specific skills are rarely required. Maintenance is made to look like an easy task, which can be done by people with low skills, without papers, long-term unemployed, ... Seemingly anyone with a weaker position in society and often it turns out to be women. Just to say something, undocumented women on the breadline have it easier to find a job than their husbands.

Work is what we do to earn money to pay access to the means we need. This is somewhat strange if one considers that everything is already at hand. Influenced by the environmental movement we learnt to look at the world as if it were finite. That's evident, but "handige" commercial "harry's" *("Handige harry" is a Dutch word for handyman, often used as the name of a character in children's television series)* also use it to promote scarcity and 'scarcity' is everywhere. According to the Lauderdale Paradox rentseekers will always try to pursue profit out of it. And good work is scarce too. Today it is a good which we consume at lower prices and slackened working conditions. The labour market translates 'crafts(wo)manship' to crafts - m/f - ship.

Professionalisation often sounds good, but since full employment became the norm, we pay a bigger price to consume work. The intimate sphere is usually the last one to be professionalised. Something as personal as maintaining a house is fixed in norms and tasks to tick off on the checklist by a certificated housekeeper. Some people clean once a week, others every day. The smell of Belpol and oiled sawdust in my parent’s garage still lingers on. Fragmentation of tasks and responsibilities in more diverse, temporary jobs by companies with practical codes of good practice takes the soul out of housekeeping. An activation labour policy with the help of "titres services" *(service vouchers)*, employment of art.60's and the reinvented idea of alternative citizen service run the risk of hollowing it out even further. In Belgium the sector of domestic help has the less worked out social agreements. In the Netherlands housekeep-st-ers *(the word 'housekeep-st-er' refers to females doing the housekeeping as the word in Dutch is written differently for males, "huishouder", and females, “huishoudster")* have their own parliament. *(In order to defend their labour conditions female housekeepers from the Netherlands created their own union called "Huishoudparlement".)*

Work frames life and many domestic tasks are outsourced, because people go to work and because there are people who want to. We let housekeeping slip from our hands. We loose the process of learning a skill and consequently we don't pass it on ourselves anymore, but we do it through "De gouden raad van Tante Kaat" and "Ons Kookboek" *("De gouden raad van Tante Kaat" and "Ons Kookboek" are popular handbooks in the Flanders that also exist in a version on the internet. An American variant is the women's magazine Good Housekeeping).* What next if even two housekeep-st-ers do not clean their own house, but each other’s and respectively pay each other with "titres services"?

We would loose our mind and senses for less than this and that is what is essential to craftsmanship. Exactly that motivation is such an important partner in delivering good work.

And yet it doesn't have to be like this. Let's act on everything that is, watch the world through a lens of abundance and not throw the baby out with the bathwater yet. Let us take on the role of the *bonus pater familias* at home and learn from all those housewives, cleaning and toilet ladies and all other handy(wo)men. Let us further develop what is already there as managers of this planet and beyond, starting from that one not yet mentioned, extraordinary multi-tasking role with that very own relationship with the built space : the janitor.

**THE JANITOR OF THE BIENNALE**

A janitor belongs to the same "Comité Paritaire" *(Joint Committee)* as domestic workers, but differs in many ways from the household workers. Firstly, he or she is a worker and not an employee. This implies a few things; a janitor is not behind closed doors and has a more autonomous position. The janitor is positioned somewhere between owner and tenants and as a kind of go-between that is always available to both parties. Where the housekeep-st-er is often considered by children as some kind of surrogate mother who does anything for them, the janitor is more like a mayor leading a specific sort of local democracy. One who could ask all house inhabitants through a three-option-poll for their opinion on new personalised mail boxes and then finally cut the knot himself. The private life of the janitor merges with the public one. His or her housekeeping is that of the building. Finally where housekeeping is often associated with women's work, this is not the case for the janitor. Janitors are often assisted by others, often by their partners. Of course it's not all rosey, the job of a janitor is not that self-evident. An obsessive ability often leads to tormented crafts(wo)manship and very stubborn janitors. We are all human, but there are definitely a lesson or two to learn here. As ultimate manager of his or her immediate environment, the janitor is linked to that environment in many possible ways. A little bit like the sha(wo)man in the wood or like the priest(ess) who cares for the temple and honours it through a ritual on the material in order to understand what's behind it. Seen from many perspectives, abundance is present and the time has come to also see it like that. Maybe by behaving like it. According to many indigenous peoples we only experience abundance when we are in balance and vice versa. Just like they feel at one with their environment, we can experience the same in our built surroundings. After all there's no difference.

This proposal for the Belgian pavilion is to be curated by a team that behaves like a janitor. In the period between the dismantlement of the Venice Art Biennale 2015 and the construction of the Architecture Biennale, traces of maintenance will be collected throughout daily activities in, on and around the building and by extension in the whole Giardini. The Belgian pavilion will act as a base. The process of maintenance will be followed up closely and eventually put on hold for nearer observation and exhibited in the same space which is conceived to. All of this will be done in close co-existence with the other inhabitants of the Giardini during this transition period. Simultaneously research will be conducted on the context of the practice of maintenance. In fact this proposal aims to mirror the personal wisdom that can be obtained in the same way that maintenance is carried out. In order to show that, we will highlight the ways in which this is made public : labour regulations and protocols, codes of good practice, but also instruments and communication tools like 'Attention ! Slippery.'-signs as well as more sensorial experiences like a repeatedly temporary dangerously shiny floor. All these elements will form the base of another, wider and more personal frame of reference for the architect. Not because of nostalgia, but for another status for the janitor today and by extension everyone who 'keeps house'.

Koen Berghmans will carry the main responsibility and will be surrounded by international team members whose home bases are Brussels.

The project is interpreted according to the logic of the Potential Office Project (POP). This platform of architects, artists and others takes action with those who are available at a given moment, quite aside of their competences, driven by their temporary engagement.

Other members are Agency, Elke Gutiérrez-Burgos (experienced expert social worker with undocumented domestic workers, informal caregivers and identity politics), Anna Rispoli (performance artist) and Livia Cahn (researcher, anthropologist), Giulia Caterina-Verga (architect) and Bernardo Robles Hidalgo (architect) for O O O.

**'Crafts(wo)manship' and the built space, Open call Belgian Pavilion Venice Architecture Biennale 2016, VAi**
