Title: Trimming buxus   
Date: 06-09-2018
Tags: video, gardening, automatic, bush
Location: WTC
Mood: still tired
Image: bruxus-trimming-machine-for-65-cm-diameter.svg

00025

In this video we see a trimming machine shaping bushes into almost perfect spheres in few seconds. This machine is mainly but not exclusively used in nursery gardens.



