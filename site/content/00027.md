Title: Chimney sweeper    
Date: 21-04-2019
Tags: children , chimney, drama, book
Location: Drama
Mood: late at night
Image: chimeney sweeper friend.svg

00027

“The Chimney-sweeper’s friend, and Climbing-boy’s album’ by James Montgomery printed in 1824 is a collection of different writings portraying the miserable conditions of child labourers, urging the authorities to take action. 

Unfortunately the industrial revolution did not bring any positive changes for these kids. Chimneys got narrower and sinuous with the use of coal which lead to more casualties. 

