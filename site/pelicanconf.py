#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'MAMA'
SITENAME = 'MAMA'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Brussels'

DEFAULT_LANG = 'en'

THEME = "themes/mama"

STATIC_PATHS = ['images', 'extra/favicon.ico']
EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'}
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None

# Social widget
SOCIAL = None

DEFAULT_PAGINATION = 250

CATEGORY_SAVE_AS = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

MENUITEMS = [
  ('office', '/office.html'),
  ('tags', '/tags.html'),
  ('print', '/print.html')
]

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

SUMMARY_MAX_LENGTH = 25

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["unify_svg_strokes"]