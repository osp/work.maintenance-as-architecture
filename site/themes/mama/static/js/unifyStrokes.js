function setSvgStrokeWidths (svg, width) {
    var els = svg.querySelectorAll('path, line');

    for (var e=0; e < els.length; e++) {
        els[e].style.strokeWidth = (width).toString() + 'px';
        els[e].style.vectorEffect = 'non-scaling-stroke';
    }
}

function unifySVGStrokeWidths (width, el) {
    if (el && el.getSVGDocument) {
        var svg = el.getSVGDocument();
        setSvgStrokeWidths(svg, width);
        // Sight overkill but Chrome does not seem to print SVG's in an object tag
        var blob = new Blob([svg.rootElement.outerHTML], { type: "image/svg+xml" }),
            url = URL.createObjectURL(blob),
            img = new Image();
        img.src = url;
        img.setAttribute('class', el.getAttribute('class'));
        el.style.display = 'none';
        img.dataset.treated = width;
        el.parentElement.appendChild(img);
    }
}

function unifyStrokeWidths () {
    var objs = document.querySelectorAll('object[type="image/svg+xml"]');
    var width = 2;
    
    for (var i=0; i < objs.length; i++) {
        if (!('treated' in objs[i].dataset) || objs[i].dataset.treated !== width) {
            if (objs[i].complete) {
                unifySVGStrokeWidths(width, objs[i]);
            }
            else {
                objs[i].addEventListener('load', (function(el) { return function () {
                    unifySVGStrokeWidths(width, el);
                }})(objs[i]), { once: true });
            }
        }
    }
}

window.unifyStrokeWidths = unifyStrokeWidths;

unifyStrokeWidths();