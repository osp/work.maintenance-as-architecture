(function () {
  'use strict';

  function clearElement(el) {
    while(el.lastChild) {
      el.removeChild(el.lastChild);
    }
  }

  var printButton = document.querySelector('#menu-item-print');

  printButton.addEventListener('click', function (e) {
    e.preventDefault();
    window.print();
  })

  window.addEventListener('beforeprint', function () {
    // Insert metadata
    var metadataEl = document.querySelector('#print-cover'),
        date = metadataEl.querySelector('.date'),
        tags = metadataEl.querySelector('.tags');

        clearElement(tags);
        clearElement(date);

        var activeTags = document.querySelectorAll('#taglist .active');
        tags.appendChild(document.createTextNode('Selected tags: '))
        for (var i=0; i< activeTags.length; i++) {
          tags.appendChild(activeTags[i].cloneNode(true));
          if ((i+1)<activeTags.length) {
            tags.appendChild(document.createTextNode(', '));
          }
        }

        var d = new Date();

        date.appendChild(document.createTextNode(
          'Generated on ' +
          d.getDate().toString(10) + '-' +
          (d.getMonth() + 1).toString(10) + '-' +
          d.getFullYear().toString(10) + ' at ' +
          d.getHours().toString(10) + ':' +
          d.getMinutes().toString(10)
        ));
  });

})();